# probot whitepapers
This repository hosts the documents for the probot contest in multiple languages.

The approved state is tagged every year just before the contest starts.

Pull requests are welcome.
