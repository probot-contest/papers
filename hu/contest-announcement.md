# PRobot

Ez a verseny arról szól, hogy ki tud jobb botot írni, azaz olyan játékost, ami emberi beavatkozás nélkül végzi a dolgát.

Remélem, hogy nyáron sokan ráérnének és érdekelne titeket egy ilyen, az érettségik/vizsgák/iskola után (ez indokolja az időpontot).

## A verseny

Adott egy **játék** (részletek lejjebb), amire **írni kell egy botprogramot** (azaz olyan játékost kell programozni, amit nem kontrollálhat ember),
**maximum 3** **fő**s csapatokban.

**Egymás ellen futnak le** a programjaitok. A beküldött megoldást **automatikusan, azonnal kiértékeljük** (ha nem terheli le a szerverünk) a többi csapat megoldása ellen, a lejátszott meccsek **utólag bármikor megtekinthetők**.

A csapatok számától függően fenntartom, hogy **több korcsoport**ra bontom a csapatokat. (pl. Egyetemista, végzős, egyfős "csapat", stb.)

*Ha nem találsz csapatot, írj (Egyedül is lehet jelentkezni, más hasonló sorsú emberekkel is össze tudlak kötni.)*

Később is be lehet csatlakozni, a regisztráció nyitott végig!

## Jelentkezés feltétele

**Dokumentációt** fogok kérni a jelentkezők jól teljesítő harmadától (várhatóan, változhat) a verseny utáni, még nem fix határidőre.

Ezen kívül a regisztráció feltétele, hogy a kódot publikáljátok egy (még nem fix) **open-source licensszel** közvetlen a verseny után GitHubon, hogy a többiek tudják, hogy milyen stratégiák működtek jól.

Ha tetszik a játék, akkor nézd végig a verseny végén mások a programját, stratégiáját. Akinek a botprogramja a legszimpatikusabb, vegyél egy tábla csokit :) (El tudom juttatni hozzá)

**Támogatott programnyelvek: C++**, kérésre bővítjük.

## A bot technikai részletei:

A játék körökre osztott, **stdin és stdout**-on kell kommunikálnia a programnak (ezek nem tényleges fájlok, hanem a System.in/System.out, std::cin/std::cout, stb. összefoglaló neve).

**Első kör elején** az állandó paramétereket kell beolvasni (Amik nem változnak futás közben).

**Minden kör elején** (az állandó paraméterek után adott esetben) csak a változó/változni képes paraméterek lesznek kihirdetve.

A konkrét játéktól függ, hogy mik a bemenetek és kimenetek, lásd [a versenyszabályzatot](problem-specification.md).

C++ nyelvhez nyújtani fogok egy példakódot, ami nem lesz túl okos, de a beolvasást elvégzi. :)


**Fejlesztéshez segítség:** Első lépésben érdemes egy objektum-orientált környezetet megvalósítani, utána erre fejleszteni algoritmusokat, hogy csapatban tudjatok fejleszteni.

**Csak saját kódot** tölthetsz fel (2 karakter Ctrl+V-zése nem számít problémának természetesen), a programnyelv általában tartalmaz megfelelő eszközöket ahhoz, hogy teljesértékű programot csinálj.

**Ne csalj: maximum 3 fő dolgozhat össze, nem szabad embernek közbeavatkozni, tilos szándékosan hibákat kihasználni a programban, akár az értékelőrendszerben, akár a játékban. neten való kommunikáció, fájl olvasása (stdin és stdout-on kívül)! Kód obfuszkációja, többszálúsítás használata, külső függvénykönyvtár használata tilos! Természetesen az értékelőrendszert feltörni, kihasználni sebezhetőségeit tilos!**

Ha hibát találsz, írj nekem.

## Határidők

Várható kezdés: 2018. 06. 25. (érettségik után)
Regisztráció nyitott a verseny alatt végig!
Várható határidő: 2018. 09. 02. (nyár vége)
Dokumentáció leadásának és programkód feltöltésének várható határideje: 2018. 09. 09.

## Köszönetnyilvánítás

A verseny ötlet és a megvalósítandó feladat is a CodinGametól jött.

Meg szeretném még köszönni sok-sok embernek a segítséget, kiemelve Lezsák Domonkost.

~ Rx7, Radnai László, radlaci97 (at) gmail (dot) com

# Jelentkezés
Ha megfelelnek a feltételek és kedvet  kaptál a versenyhez, itt megteheted: [Google  Forms](https://docs.google.com/forms/d/e/1FAIpQLSeEWECgoOn5h2SP9TyWaKhuG5ECNylXk-xbICcA4md7nomePw/viewform?usp=sf_link)
