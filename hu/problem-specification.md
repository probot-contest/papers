# Probot - a játék leírása

A leírás még nem végleges!

A játékot **két játékos** játssza. A játék körökre osztott, célja a másik bázisának elfoglalása. Ha ez nem sikerül 500 körig, akkor az nyer, akinek a legtöbb területe van. (Döntetlennel is végződhet meccs)

**Pálya:**
A játék véletlenszerűen (seed alapján) generált pályán játszódik. Egy négyzethálón helyezkednek el a mezők. A 2 játékos irányítja a pályán az egységeiket és egymás ellen harcolnak.

**Terület:**
A játék elején mindkét játékosnak van egy-egy területe, a többi semleges. A két terület a két játékos bázisa. Egy területet úgy foglalhatunk el, hogy nincs rajta ellenfél és ráviszünk egy egységet. Ha van rajta ellenfél, akkor nem feltétlen változik a tulajdonosa a területnek. (lásd lentebb a pontosabb szabályokat)

**Arany:**
Minden területnek lehet 0-6 arany közötti termelése. Minden elfoglalt terület után megkapja a játékos a kör végén a termelést.

**Egység:**
A játékosok ezeket tudják irányítani szomszédos mezőkre. A területek elfoglalását az egységek irányításával tudjuk elérni. Az elfoglalt területek után kap a játékos aranyat; 10 aranyért lehet venni új egységet.

**Csata:**
Ha egy területen mindkét játékosnak  van egysége, akkor legfeljebb 3 egység megsemmisül mindkét féltől. (lásd lentebb)

Technikailag egy kör 6 részből fog állni:
1. ÁLLAPOTFRISSÍTÉS: A pálya állapotának elküldése a játékosnak. Az botprogram stdin (`scanf`, `cin`, `System.in`, stb. használatával elérhető) bemenetére kerülnek az adatok lejjebb specifikált módon.
2. MOZGÁS: minden egység elmozdul a botprogram stdout (`printf`, `cout`, `System.out`, stb. használatával elérhető) kimenete alapján. Helytelen utasítás esetén automatikusan veszít a játékos.
3. GYÁRTÁS: minden 10 aranyért 1-1 egység automatikusan legyártásra kerül a bázison.
4. TERMELÉS: minden a játékos által birtokolt területre megkapja az aranytermelést.
5. CSATÁZÁS: Ha egy területen mindkét játékosnak van egysége, akkor csatáznak, ekkor mindkét féltől ugyanannyi egység megsemmisül. (Lásd (még) lentebb)
6. FOGLALÁS: Ha egy területen az egyik játékosnak van egysége, a másiknak nincs, akkor az előbbi megkapja a területet. (Nem változik egyébként a terület birtokosa)

## Részletes szabályok

### ÁLLAPOTFRISSÍTÉS
A bemenet tekinthető egy szöveges fájlnak, amiben egész számok vannak. Ezek egyetlen szóközzel vannak elválasztva.

**Az első körben** ezekkel kezdjük az adatsort:

Az első sorban azt adjuk meg, hogy mi a játékos sorszáma (első vagy második), a területek N száma, az első és a második játékos bázisának sorszáma és az összeköttetések (szomszédos területek) M száma található.

A második sorban N szám található: az i. szám az i. mező termelését adja meg.

Harmadik sortól M sorban minden sor egy *a* és egy *b* számot tartalmaz, ami azt jelenti, hogy egység *a* mezőből *b* mezőbe tud menni és fordítva.


**Az összes körben továbbá:**

Az első (vagy első kör esetén sokadik) sorban található az aranyad mennyisége a kör elején.

A következő N (területek száma) sorban három szám található ebben a sorrendben: a tulajdonosa az i. mezőnek, az első játékos, illetve a második játékos egységeinek a száma.

### MOZGÁS
Az egységeket a játékos tudja mozgatni. Egy körben egy sort kell a botprogramnak küldenie, ami számokat tartalmaz, a végén a sort `-1`-gyel lezárva.

A számok hármasával értelmezndők: Hány, honnan, hova. Kivéve a sorzáró `-1`, amihez nem tartozik két másik szám.

Azaz a `2 5 9 1 2 3 -1` kimenet azt jelenti, hogy 2 egységet 5-ből 9-be, 1 egységet 2-ből 3-ba akar küldeni a játékos.

Ha a lépés illegális, vagy nem követi a protokollt a játékos, akkor automatikusan veszíti a játékot.

### GYÁRTÁS
Minden 10 arany becserélődik egy egységre a bázisban. Azaz, ha 53 aranya volt az egyik játékosnak, a bázisan 6 egységgel, akkor 11 egysége lesz ezután a bázison és 3 aranya marad.

### TERMELÉS
Az aranya megnő a játékosnak azzal a mennyiséggel, amennyi össztermelése van a jelenleg birtokolt területeinek.

### CSATÁZÁS

Ha egy területen két játékosnak is van egysége, akkor legfeljebb 3 egység megsemmisül mindkét féltől. 3 lépésben megsemmisül egy-egy egység mindkét féltől, ha még tart a csata (azaz mindkét játékosnak maradt ott egysége).

Azaz, ha _a_ és _b_ egysége van az első és a második játékosnak, akkor _a &ge; 3_ és _b &ge; 3_ esetén _a - 3_ és _b - 3_ egység marad, egyébként _0_ és _b - a_ egység marad, feltéve, hogy _a < b_.

Például, ha 3 és 5 egység volt, akkor a 0 és 2 lesz (ezutáni fázisnál a második játékos megszerzi a területet)

Ha 1 és 11 egység volt, akkor 0 és 10 lesz. (és a második megszerzi a területet)

Ha 2 és 1 egység volt a területen, akkor 1 és 0 lesz (ezután az első szerzi meg a területet)

### FOGLALÁS
Minden területre:

1. ha nincs rajta egység, nem változik a birtokosa
2. ha csak az egyik játékostól van egység, akkor az eredeti birtokostól függetlenül az egység birtokosa kapja meg a területet.

## Játék vége

A játék automatikusan véget ér (a botprogramokat leállítjuk), amint a végfeltétel teljesül: az egyik bázisa megsemmisül (azaz a másik elfoglalja) vagy befejeződik az 500. kör.

A játék végén az nyer, aki a másik bázisát elfoglalta, vagy, ha nincs ilyen, akkor az, akinek több területe van. 2, 1 vagy 0 pontot lehet szerezni. (döntetlen esetén 1-1, egyébként 2-0 vagy 0-2)
